﻿using System;
namespace SeanDuffy.Common.Interfaces
{
    public interface IExceptionPolicy
    {
        /// <summary>
        /// Provides a mechanism for handling exceptions
        /// </summary>
        /// <param name="exception">The exception to handle</param>
        /// <param name="exceptionPolicyKey">Any policy key required to facilitate handling</param>
        /// <returns></returns>
        bool HandleException(Exception exception, string exceptionPolicyKey);
    }
}
