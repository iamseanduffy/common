﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using SeanDuffy.Common.Interfaces;

namespace SeanDuffy.Common.WebServices
{
    [Serializable]
    [DataContract]
    public abstract class WebServiceAdaptorBase
    {
        protected TimeSpan _executionTime;
        protected string _messageException;
        protected Exception _exception;

        [DataMember]
        public string ExecutionTime
        {
            get
            {
                return String.Format("{0:00}:{1:00}:{2:00}.{3:00}", _executionTime.Hours,
                    _executionTime.Minutes, _executionTime.Seconds, _executionTime.Milliseconds/10);
            }
        }

        [DataMember]
        public string MessageException
        {
            get { return _messageException; }
        }

        [XmlIgnore]
        public Exception Exception
        {
            get { return _exception; }
        }



        public IExceptionPolicy ExceptionPolicy;
        public bool IsSuccess { get; protected set; }

        protected internal virtual string ExpceptionPolicyKey
        {
            get { return "ExceptionPolicy"; }
        }

        protected internal Stopwatch _sw;

        protected void ProcessResponse(Exception ex)
        {
            _sw.Stop();
            _executionTime = _sw.Elapsed;
            IsSuccess = true;

            if (ex == null) return;

            IsSuccess = false;
            _exception = ex;
            _messageException = ex.Message;
            ex.Data.Add("ExecutionTime", _sw.Elapsed);
        }
    }
}