﻿namespace SeanDuffy.Common.WebServices
{
    public class VerboseResult<TRequest, TResponse>
    {
        private readonly WebServiceAdaptorBase _webServiceAdaptorBase;

        public VerboseResult(WebServiceAdaptorBase analysis)
        {
            _webServiceAdaptorBase = analysis;
        }

        #region Implementation of IServiceCallResult<TRequest,TResponse>

        public TRequest MessageRequest { get; set; }
        public TResponse MessageResponse { get; set; }

        public WebServiceAdaptorBase WebServiceAdaptorBase
        {
            get { return _webServiceAdaptorBase; }
        }

        #endregion
    }

    public class VerboseResult<TRequest>
    {
        private readonly WebServiceAdaptorBase _webServiceAdaptorBase;

        public VerboseResult(WebServiceAdaptorBase analysis)
        {
            _webServiceAdaptorBase = analysis;
        }

        #region Implementation of IServiceCallResult<TRequest,TResponse>

        public TRequest MessageRequest { get; set; }

        public WebServiceAdaptorBase WebServiceAdaptorBase
        {
            get { return _webServiceAdaptorBase; }
        }

        #endregion
    }

    public class VerboseResultNoRequest<TResponse>
    {
        private readonly WebServiceAdaptorBase _webServiceAdaptorBase;

        public VerboseResultNoRequest(WebServiceAdaptorBase analysis)
        {
            _webServiceAdaptorBase = analysis;
        }

        #region Implementation of IServiceCallResult<TRequest,TResponse>

        public TResponse MessageResponse { get; set; }

        public WebServiceAdaptorBase WebServiceAdaptorBase
        {
            get { return _webServiceAdaptorBase; }
        }

        #endregion

    }
}