﻿using System;
using System.Diagnostics;
using SeanDuffy.Common.Extensions;

namespace SeanDuffy.Common.WebServices
{
    public class WebServiceAdaptor<TRequest, TResponse> : WebServiceAdaptorBase
    {
        public VerboseResult<TRequest, TResponse> VerboseResult { get; private set; }

        public delegate TResponse ProcessRequestDelegate(TRequest requestMethod);

        public TResponse Process(TRequest request,
            ProcessRequestDelegate processRequestDelegate)
        {
            _sw = Stopwatch.StartNew();
            var response = default(TResponse);

            try
            {
                VerboseResult = new VerboseResult<TRequest, TResponse>(this);

                response = processRequestDelegate(request);
                FinaliseVerboseResult(request, response);
                return response;
            }
            catch (Exception ex)
            {
                try
                {
                    FinaliseVerboseResult(request, response, ex);
                    if (ExceptionPolicy != null)
                        ExceptionPolicy.HandleException(ex, ExpceptionPolicyKey);
                }
                catch
                {
                    IsSuccess = false;
                }

                return default(TResponse);
            }
        }

        private void FinaliseVerboseResult(TRequest request,
            TResponse response,
            Exception ex = null)
        {
            ProcessResponse(ex);
            VerboseResult.MessageRequest = request;
            VerboseResult.MessageResponse = response;

            if (ex == null) return;
            ex.Data.Add("MessageRequest", request);

            if (response is ValueType ||
                response == null) return;

            ex.Data.Add("MessageResponse", response.SerialiseToString());
        }
    }

    public class WebServiceAdaptor<TRequest> : WebServiceAdaptorBase
    {
        public delegate void ProcessRequestDelegate(TRequest requestMethod);

        public VerboseResult<TRequest> VerboseResult { get; private set; }

        public void Process(TRequest request, ProcessRequestDelegate processRequestDelegate)
        {

            _sw = Stopwatch.StartNew();

            try
            {
                VerboseResult = new VerboseResult<TRequest>(this);

                processRequestDelegate(request);
                FinaliseVerboseResult(request);
            }
            catch (Exception ex)
            {
                try
                {
                    FinaliseVerboseResult(request, ex);
                    if (ExceptionPolicy != null)
                        ExceptionPolicy.HandleException(ex, ExpceptionPolicyKey);
                }
                catch
                {
                    IsSuccess = false;
                }
            }
        }

        private void FinaliseVerboseResult(TRequest request, Exception ex = null)
        {
            ProcessResponse(ex);
            VerboseResult.MessageRequest = request;
            if (ex == null) return;
            ex.Data.Add("MessageRequest", request);
        }
    }

    public class WebServiceAdaptorNoRequest<TResponse> : WebServiceAdaptorBase
    {
        public delegate TResponse ProcessRequestDelegate();

        public VerboseResultNoRequest<TResponse> VerboseResult { get; private set; }

        public TResponse Process(ProcessRequestDelegate processRequestDelegate)
        {
            _sw = Stopwatch.StartNew();
            var response = default(TResponse);

            try
            {
                VerboseResult = new VerboseResultNoRequest<TResponse>(this);
                response = processRequestDelegate();
                FinaliseVerboseResult(response);
                return response;
            }
            catch (Exception ex)
            {
                try
                {
                    FinaliseVerboseResult(response, ex);
                    if (ExceptionPolicy != null)
                        ExceptionPolicy.HandleException(ex, ExpceptionPolicyKey);
                }
                catch
                {
                    IsSuccess = false;
                }

                return default(TResponse);
            }
        }

        private void FinaliseVerboseResult(TResponse response,
            Exception ex = null)
        {
            ProcessResponse(ex);
            VerboseResult.MessageResponse = response;
            if (ex == null) return;

            if (response is ValueType ||
                response == null) return;

            ex.Data.Add("MessageResponse", response.SerialiseToString());
        }
    }
}