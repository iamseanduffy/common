﻿namespace SeanDuffy.Common.Configuration
{
   internal interface IConfigReader
   {
      bool TryGetValue(string key, out string value);
   }
}