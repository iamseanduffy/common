﻿using System.Collections.Generic;
using System.Dynamic;

namespace SeanDuffy.Common.Configuration
{
    public class DynamicConfig<T> where T : class 
    {
        private readonly IConfigReader _configReader;

        public DynamicConfig() : this(null)
        {
        }

        internal DynamicConfig(
            IConfigReader configReader)
        {
            _configReader = configReader ?? new ConfigReader();
        }

        public T GenerateConfig(bool allowIncompleteConfig = false)
        {
            var interfacePropertyExtractor = new InterfacePropertyExtractor();
            var propertyInfoDtos = interfacePropertyExtractor.GetPropertyInfoDtosFromType(typeof(T));
            var propertyInfoDtoValidator = new ConfigurationPropertyInfoValidator();
            propertyInfoDtoValidator.Validate(propertyInfoDtos);

            PopulateGetInfoValuesFromConfig(propertyInfoDtos, allowIncompleteConfig);
            var dynamicType = CreateDynamicType(propertyInfoDtos, allowIncompleteConfig);

            return DynamicDuck.DynamicDuck.AsIf<T>(dynamicType);
        }

        private dynamic CreateDynamicType(IEnumerable<ConfigurationPropertyInfo> propertyInfoDtos,
            bool allowIncompleteConfig)
        {
            var dynamicType = new ExpandoObject();

            foreach (var propertyInfoDto in propertyInfoDtos)
            {
                if (!string.IsNullOrEmpty(propertyInfoDto.PropertyValue))
                {
                    var invalidParse = false;

                    if (propertyInfoDto.PropertyType == typeof (bool))
                    {
                        bool boolValue;
                        if (propertyInfoDto.PropertyValue == "0")
                            propertyInfoDto.PropertyValue = "false";
                        else if (propertyInfoDto.PropertyValue == "1")
                            propertyInfoDto.PropertyValue = "true";

                        var isValidBoolValue = bool.TryParse(propertyInfoDto.PropertyValue, out boolValue);
                        if (isValidBoolValue)
                            ((IDictionary<string, object>)dynamicType).Add(propertyInfoDto.PropertyName, boolValue);
                        else
                            invalidParse = true;
                    }
                    else if (propertyInfoDto.PropertyType == typeof (int))
                    {
                        int intValue;
                        var isValidIntValue = int.TryParse(propertyInfoDto.PropertyValue, out intValue);
                        if (isValidIntValue)
                            ((IDictionary<string, object>)dynamicType).Add(propertyInfoDto.PropertyName, intValue);
                        else
                            invalidParse = true;
                    }
                    else if (propertyInfoDto.PropertyType == typeof (string))
                    {
                        if (!string.IsNullOrEmpty(propertyInfoDto.PropertyValue))
                            ((IDictionary<string, object>)dynamicType).Add(propertyInfoDto.PropertyName, propertyInfoDto.PropertyValue);
                        else
                            invalidParse = true;
                    }
                    else
                        throw new DynamicConfigException(
                            string.Format("The type '{0}' is not supported", propertyInfoDto.PropertyType));

                    if (invalidParse)
                        throw new DynamicConfigException(
                            string.Format("Config setting '{0}' for property '{1}' cannot be parsed to type '{2}'"
                                          , propertyInfoDto.PropertyValue, propertyInfoDto.PropertyName,
                                          propertyInfoDto.PropertyType));
                }
                else if (!allowIncompleteConfig)
                {
                    throw new DynamicConfigException(
                        string.Format("Key '{0}' is not present in the config and incomplete config is not allowed.",
                                      propertyInfoDto.PropertyName));
                }
            }

            return dynamicType;
        }

        private void PopulateGetInfoValuesFromConfig(IEnumerable<ConfigurationPropertyInfo> propertyInfoDtos, bool allowIncompleteConfig)
        {
            foreach (var propertyInfoDto in propertyInfoDtos)
            {
                string propertyValue;
                var isValidValue = _configReader.TryGetValue(propertyInfoDto.PropertyName, out propertyValue);

                if ((!isValidValue ||
                     string.IsNullOrEmpty(propertyValue)) &&
                    !allowIncompleteConfig)
                {
                    throw new DynamicConfigException(
                        string.Format("Key '{0}' is not present in the config and incomplete config is not allowed.",
                                      propertyInfoDto.PropertyName));
                }

                propertyInfoDto.PropertyValue = propertyValue;
            }
        }
    }
}