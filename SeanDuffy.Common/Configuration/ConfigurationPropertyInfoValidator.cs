﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SeanDuffy.Common.Configuration
{
   public class ConfigurationPropertyInfoValidator
   {
      private readonly List<Type> _supportedTypes = new List<Type>
         {
            typeof (bool),
            typeof (int),
            typeof (string)
         };

      public void Validate(List<ConfigurationPropertyInfo> propertyInfoDtos)
      {
         if ((propertyInfoDtos == null) ||
             !propertyInfoDtos.Any())
            throw new DynamicConfigException("The propertyInforDtos is either null or empty");

         foreach (var propertyInfoDto in propertyInfoDtos
            .Where(propertyInfoDto => !_supportedTypes.Contains(propertyInfoDto.PropertyType)))
             throw new DynamicConfigException
               (string.Format("The type '{0}' is not supported", propertyInfoDto.PropertyType));
      }
   }
}
