﻿using System;

namespace SeanDuffy.Common.Configuration
{
   public class ConfigurationPropertyInfo
   {
      public string PropertyName { get; set; }
      public Type PropertyType { get; set; }
      public string PropertyValue { get; set; }
   }
}
