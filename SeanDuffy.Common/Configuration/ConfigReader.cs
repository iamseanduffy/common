﻿using System.Configuration;

namespace SeanDuffy.Common.Configuration
{
   public class ConfigReader : IConfigReader
   {
      public bool TryGetValue(string key, out string value)
      {
         value = ConfigurationManager.AppSettings[key];
         return value != null;
      }
   }
}
