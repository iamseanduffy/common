﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SeanDuffy.Common.Configuration
{
   public class InterfacePropertyExtractor
   {
      public List<ConfigurationPropertyInfo> GetPropertyInfoDtosFromType(Type type)
      {
         if (!type.IsInterface)
            throw new DynamicConfigException(string.Format("'{0}' is not an interface", type));

         var hasUnsupportedMethods = type.GetMethods()
            .Any(_ => !_.Name.StartsWith("get_") && !_.Name.StartsWith("set_"));

         if (hasUnsupportedMethods)
             throw new DynamicConfigException(string.Format("Interface '{0}' must not contain methods",
                                                                           type));
         return type.GetProperties()
            .Select(propertyInfo => new ConfigurationPropertyInfo
            {
               PropertyName = propertyInfo.Name,
               PropertyType = propertyInfo.PropertyType
            }).ToList();
      }
   }
}