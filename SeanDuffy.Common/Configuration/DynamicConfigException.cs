﻿using System;

namespace SeanDuffy.Common.Configuration
{ 
   public class DynamicConfigException : Exception
   {
       public DynamicConfigException(string message) : base(message) { }
   }
}

