﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace SeanDuffy.Common.Extensions
{
    public static class ExtensionMethods
    {
        public static string JoinAsString<T>(this IEnumerable<T> sequence, string separator)
        {
            return String.Join(separator, sequence);
        }

        public static bool IsIn(this Enum enumeration, params Enum[] enumerations)
        {
            return enumerations.Contains(enumeration);
        }

        public static bool ContainsAttributesDerivedFromAnyOf(this IEnumerable<Attribute> me,
            ICollection<Type> attributeTypes)
        {
            return me.GetAttributesDerivedFrom(attributeTypes).Any();
        }

        public static IEnumerable<Attribute> GetAttributesDerivedFrom(this IEnumerable<Attribute> me,
            ICollection<Type> attributeTypes)
        {
            return me.Where(attribute => attribute.IsInstanceOfAnyType(attributeTypes));
        }

        public static bool IsInstanceOfAnyType(this object me, ICollection<Type> types)
        {
            if (me == null) return false;

            var thisType = me.GetType();
            var baseTypes = types.Where(baseType => baseType.IsAssignableFrom(thisType));

            return baseTypes.Any();
        }

        public static string ToDescriptionString(this Enum enumVal)
        {
            if (enumVal != null)
            {
                var attributes = enumVal.GetCustomAttribute<DataMemberAttribute>();
                return attributes == null ? enumVal.ToString() : attributes.Name;
            }

            return string.Empty;
        }

        public static string ToDescriptionString(this Enum enumVal, params object[] formatParameters)
        {
            return string.Format(enumVal.ToDescriptionString(), formatParameters);
        }

        public static T[] GetCustomAttributes<T>(this Enum enumVal)
            where T : Attribute
        {
            return
                enumVal.GetType()
                    .GetField(enumVal.ToString())
                    .GetCustomAttributes(typeof (T), false)
                    .Cast<T>()
                    .ToArray();
        }

        public static T GetCustomAttribute<T>(this Enum enumVal)
            where T : Attribute
        {
            return GetCustomAttributes<T>(enumVal).FirstOrDefault();
        }
    }
}