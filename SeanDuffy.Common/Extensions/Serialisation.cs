﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace SeanDuffy.Common.Extensions
{
    public static class Serialisation
    {
        public static string SerialiseToString<T>(this T theObject)
        {
            if (theObject is ValueType ||
                theObject == null)
                throw new ArgumentNullException();

            string returnValue;
            var xmlSerializer = new XmlSerializer(theObject.GetType());
            StringWriter writer = null;
            try
            {
                using (writer = new StringWriter())
                {
                    xmlSerializer.Serialize(writer, theObject);
                    returnValue = writer.ToString();
                }
            }
            finally
            {
                if (writer != null)
                {
                    writer.Close();
                    writer.Dispose();
                }
            }
            return returnValue;
        }
    }
}