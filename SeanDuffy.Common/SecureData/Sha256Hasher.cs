﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace SeanDuffy.Common.SecureData
{
    public class Sha256Hasher
    {
        public string GenerateHash(string clearText, string salt)
        {
            var plainSaltedText = String.Concat(clearText, salt);
            var algorithm = SHA256.Create();
            var plainTextBytes = Encoding.UTF8.GetBytes(plainSaltedText);
            var data = algorithm.ComputeHash(plainTextBytes);
            var hashedText = string.Empty;

            for (var i = 0; i < data.Length; i++)
                hashedText += data[i]
                    .ToString("x2")
                    .ToUpperInvariant();

            return hashedText;
        }

        public bool IsMatching(string knownHash,
            string clearText,
            string salt)
        {
            var hashedText = GenerateHash(clearText, salt);
            return hashedText == knownHash;
        }
    }
}
