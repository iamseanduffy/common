﻿using System;
using System.Security.Cryptography;

namespace SeanDuffy.Common.SecureData
{
    public static class Salt
    {
        public static string Generate()
        {
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[12];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }
    }
}
