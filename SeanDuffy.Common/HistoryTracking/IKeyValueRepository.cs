﻿namespace SeanDuffy.Common.HistoryTracking
{
    public interface IKeyValueRepository
    {
        void Save(string key, string value);
        string GetByKey(string key);
        void DeleteByKey(string key);
    }
}