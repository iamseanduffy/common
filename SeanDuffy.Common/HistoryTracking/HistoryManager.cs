﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace SeanDuffy.Common.HistoryTracking
{
    internal class HistoryManager : IHistoryManager
    {
        public HistoryManager(string historyTypeName) : this(null, historyTypeName)
        {
        }

        public HistoryManager(IKeyValueRepository keyValueRepository, string historyTypeName)
        {
            _historyTypeName = historyTypeName;
            _keyValueRepository = keyValueRepository ?? new RegistryKeyValueRepository(_historyTypeName);
        }

        private readonly IKeyValueRepository _keyValueRepository;
        private readonly string _historyTypeName;
        private const char SplitChar = '|';
        internal const int MaxItems = 9;

        public string HistoryTypeName
        {
            get { return _historyTypeName; }
        }

        public Stack<string> GetHistory()
        {
            var historyString = _keyValueRepository.GetByKey(_historyTypeName);

            if (string.IsNullOrEmpty(historyString) == false)
            {
                var historyArray = historyString
                    .Split(SplitChar)
                    .Where(s => string.IsNullOrEmpty(s) == false)
                    .ToArray();

                return new Stack<string>(historyArray.Reverse());
            }

            return new Stack<string>();
        }

        public void AddHistoryItem(string newItem)
        {
            if (string.IsNullOrEmpty(newItem))
            {
                throw new ArgumentNullException("newItem");
            }

            var historyQueue = GetHistory();

            if (historyQueue.Contains(newItem))
            {
                historyQueue = new Stack<string>(historyQueue.Where(item => item != newItem).Reverse());
            }

            historyQueue.Push(newItem);

            if (historyQueue.Count > MaxItems)
            {
                historyQueue.Pop();
            }

            var historyStringArray = historyQueue.ToArray();
            _keyValueRepository.Save(_historyTypeName, string.Join(SplitChar.ToString(CultureInfo.InvariantCulture), historyStringArray));
        }

        public void ClearHistory()
        {
            _keyValueRepository.DeleteByKey(_historyTypeName);
        }
    }
}