﻿using System.Collections.Generic;

namespace SeanDuffy.Common.HistoryTracking
{
   public interface IHistoryManager
   {
      string HistoryTypeName { get; }
      Stack<string> GetHistory();
      void AddHistoryItem(string newItem);
      void ClearHistory();
   }
}
