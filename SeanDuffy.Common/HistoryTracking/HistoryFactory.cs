﻿using System.Collections.Generic;
using System.Linq;

namespace SeanDuffy.Common.HistoryTracking
{
    public static class HistoryFactory
    {
        private static readonly object lockObject = new object();
        private static List<IHistoryManager> _historyManagers;

        public static IHistoryManager GetInstance(string historyTypeName)
        {
            lock (lockObject)
            {
                if ((_historyManagers == null) || 
                    (_historyManagers.Count == 0) ||
                    (_historyManagers.Any(_ => _.HistoryTypeName == historyTypeName) == false))
                {
                    if (_historyManagers == null)
                        _historyManagers = new List<IHistoryManager>();
                    
                    _historyManagers.Add(new HistoryManager(historyTypeName));
                }

                return _historyManagers.First(_ => _.HistoryTypeName == historyTypeName);
            }
        }
    }
}
