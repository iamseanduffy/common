﻿using Microsoft.Win32;

namespace SeanDuffy.Common.HistoryTracking
{
    public class RegistryKeyValueRepository : IKeyValueRepository
    {
        public RegistryKeyValueRepository(string registryKeyPath)
        {
            _registryKeyPath = registryKeyPath;
        }

        private readonly string _registryKeyPath;

        public void Save(string key, string value)
        {
            var registryKey = Registry.CurrentUser.OpenSubKey(string.Format("{0}{1}", _registryKeyPath, key), true) ??
                              Registry.CurrentUser.CreateSubKey(string.Format("{0}{1}", _registryKeyPath, key));

            if (registryKey != null)
            {
                registryKey.SetValue(key, value);
            }
        }

        public string GetByKey(string key)
        {
            var registryKey = Registry.CurrentUser.OpenSubKey(string.Format("{0}{1}", _registryKeyPath, key)) ??
                              Registry.CurrentUser.CreateSubKey(string.Format("{0}{1}", _registryKeyPath, key));

            if ((registryKey != null) &&
                (registryKey.GetValue(key) != null))
            {
                return registryKey.GetValue(key).ToString();
            }

            return string.Empty;
        }

        public void DeleteByKey(string key)
        {
            if (Registry.CurrentUser.OpenSubKey(string.Format("{0}{1}", _registryKeyPath, key)) != null)
            {
                Registry.CurrentUser.DeleteSubKey(string.Format("{0}{1}", _registryKeyPath, key));
            }
        }
    }
}
