﻿using System.Collections.Generic;
using Moq;
using SeanDuffy.Common.HistoryTracking;

namespace SeanDuffy.Common.Tests.Unit.HistoryTracking
{
    internal class TestableHistoryManager : HistoryManager
    {
        private TestableHistoryManager(IKeyValueRepository keyValueRepository) : base(keyValueRepository, "Test")
        {
        }

        private string _concatinatedValue = "";
        private readonly Dictionary<string, string> _inMemoryDataStore = new Dictionary<string, string>();

        public static TestableHistoryManager Create()
        {
            var keyValueRepositoryMock = new Mock<IKeyValueRepository>();
            var historyManager = new TestableHistoryManager(keyValueRepositoryMock.Object);
            keyValueRepositoryMock.Setup(repo => repo.GetByKey(It.IsAny<string>())).Returns(() => historyManager._concatinatedValue);
            keyValueRepositoryMock.Setup(repo => repo.Save(It.IsAny<string>(), It.IsAny<string>())).Callback<string, string>((key, value) => historyManager.UpdateInMemoryDataStore(value));
            return historyManager;
        }

        private void UpdateInMemoryDataStore(string value)
        {
            if (_inMemoryDataStore.ContainsKey("Test") == false)
            {
                _inMemoryDataStore.Add("Test", value);
            }
            else
            {
                _inMemoryDataStore["Test"] = value;
            }

            _concatinatedValue = _inMemoryDataStore["Test"];
        }
    }
}
