﻿using System;
using System.Linq;
using Moq;
using SeanDuffy.Common.HistoryTracking;
using Xunit;

namespace SeanDuffy.Common.Tests.Unit.HistoryTracking
{
    public class HistoryManagerTests
    {
        [Fact]
        public void AddHistoryItem_NoLastUsed_ThrowsArgumentNullException()
        {
            // Arrange
            var keyValueRepositoryMock = new Mock<IKeyValueRepository>();
            var keyValueRepository = keyValueRepositoryMock.Object;
            var historyManager = new HistoryManager(keyValueRepository, "Test");

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => historyManager.AddHistoryItem(string.Empty));
        }

        [Fact]
        public void GetHistory_PopulatedCollection_MostRecent()
        {
            // Arrange
            const string expected = "test12";
            var historyManager = TestableHistoryManager.Create();
            historyManager.AddHistoryItem("test3");
            historyManager.AddHistoryItem("test5");
            historyManager.AddHistoryItem("test12");

            // Act
            var actual = historyManager.GetHistory().ToArray()[0];

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetHistory_AddMoreThanMaximumAllowed_ItemCoundDoesNotExceedMaximum()
        {
            // Arrange
            const int expected = HistoryManager.MaxItems;
            var historyManager = TestableHistoryManager.Create();
            historyManager.AddHistoryItem("1");
            historyManager.AddHistoryItem("2");
            historyManager.AddHistoryItem("3");
            historyManager.AddHistoryItem("4");
            historyManager.AddHistoryItem("5");
            historyManager.AddHistoryItem("6");
            historyManager.AddHistoryItem("7");
            historyManager.AddHistoryItem("8");
            historyManager.AddHistoryItem("9");
            historyManager.AddHistoryItem("10");
            historyManager.AddHistoryItem("11");
            historyManager.AddHistoryItem("12");
            historyManager.AddHistoryItem("13");

            // Act
            var actual = historyManager.GetHistory().Count();

            // Assert
            Assert.Equal(expected, actual);
        }
        
        [Fact]
        public void GetHistory_PopulatedCollectionWithDuplicateAdded_OnlyOnceInstanceOfDuplicate()
        {
            // Arrange
            const int expected = 1;
            var historyManager = TestableHistoryManager.Create();
            historyManager.AddHistoryItem("test3");
            historyManager.AddHistoryItem("test3");
            historyManager.AddHistoryItem("test12");

            // Act
            var actual = historyManager.GetHistory().Count(_ => _ == "test3");

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}