﻿using System.Runtime.Serialization;
using SeanDuffy.Common.Extensions;
using Xunit;

namespace SeanDuffy.Common.Tests.Unit.Extensions
{
    public class SerialisationTests
    {
        [Fact]
        public void SerializeToString_ValidType_ReturnsCorrectSerialisedString()
        {
            // Arrange
            var testCase = new SerializationTestClass
                {
                    Name = "Testname", 
                    Age = 60
                };

            // Act
            var actual = testCase.SerialiseToString();

            // Assert
            Assert.NotEqual(string.Empty, actual);
        }
    }

    public class SerializationTestClass
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int Age { get; set; }
    }
}
