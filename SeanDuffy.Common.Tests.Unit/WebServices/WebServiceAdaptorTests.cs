﻿using System;
using System.Web.Services.Protocols;
using System.Xml;
using Moq;
using SeanDuffy.Common.Interfaces;
using SeanDuffy.Common.WebServices;
using Xunit;

namespace SeanDuffy.Common.Tests.Unit.WebServices
{
    public class WebServiceAdaptorTests
    {
        private const string ExpectedWebServiceResult = "ok";

        [Fact]
        public void Process_SuccessfulCall_ReturnsValueFromCall()
        {
            // Arrange
            const string expected = ExpectedWebServiceResult;
            var webServiceAdaptor = new WebServiceAdaptor<int, string>();

            // Act
            var actual = webServiceAdaptor.Process(4, MockSuccessfulWebServiceCall);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Process_SuccessfullCall_IsSuccessTrue()
        {
            // Arrange
            var webServiceAdaptor = new WebServiceAdaptor<int, string>();

            // Act
            webServiceAdaptor.Process(4, MockSuccessfulWebServiceCall);
            var actual = webServiceAdaptor.IsSuccess;

            // Assert
            Assert.True(actual);
        }

        [Fact]
        public void Process_FailedCall_IsSuccessFalse()
        {
            // Arrange
            var webServiceAdaptor = new WebServiceAdaptor<int, string>();

            // Act
            webServiceAdaptor.Process(4, MockFailedWebServiceCall);
            var actual = webServiceAdaptor.IsSuccess;

            // Assert
            Assert.False(actual);
        }

        [Fact]
        public void Process_FailedCall_ExceptionPolicyCalled()
        {
            // Arrange
            var webServiceAdaptor = new WebServiceAdaptor<int, string>();
            var exceptionPolicyMock = new Mock<IExceptionPolicy>();
            var exceptionPolicy = exceptionPolicyMock.Object;
            var expected = webServiceAdaptor.ExpceptionPolicyKey;
            string actualExceptionPolicyKey = null;

            exceptionPolicyMock.Setup(policy => policy.HandleException(It.IsAny<Exception>(), It.IsAny<string>()))
                .Callback<Exception, string>((e, s) => actualExceptionPolicyKey = s);

            webServiceAdaptor.ExceptionPolicy = exceptionPolicy;

            // Act
            webServiceAdaptor.Process(4, MockFailedWebServiceCall);

            // Assert
            Assert.Equal(expected, actualExceptionPolicyKey);
        }

        [Fact]
        public void Process_SuccessfullCallToVoidReturnService_IsSuccessTrue()
        {
            // Arrange
            var webServiceAdaptor = new WebServiceAdaptor<int>();

            // Act
            webServiceAdaptor.Process(4, MockSuccessfulWebServiceCallVoidReturn);
            var actual = webServiceAdaptor.IsSuccess;

            // Assert
            Assert.True(actual);
        }

        [Fact]
        public void Process_FailedCallToVoidReturnService_IsSuccessFalse()
        {
            // Arrange
            var webServiceAdaptor = new WebServiceAdaptor<int>();

            // Act
            webServiceAdaptor.Process(4, MockFailedWebServiceCallVoidReturn);
            var actual = webServiceAdaptor.IsSuccess;

            // Assert
            Assert.False(actual);
        }

        [Fact]
        public void Process_FailedCallToVoidReturnService_ExceptionPolicyCalled()
        {
            // Arrange
            var webServiceAdaptor = new WebServiceAdaptor<int>();
             var exceptionPolicyMock = new Mock<IExceptionPolicy>();
            var exceptionPolicy = exceptionPolicyMock.Object;
            var expected = webServiceAdaptor.ExpceptionPolicyKey;
            string actualExceptionPolicyKey = null;

            exceptionPolicyMock.Setup(policy => policy.HandleException(It.IsAny<Exception>(), It.IsAny<string>()))
                .Callback<Exception, string>((e, s) => actualExceptionPolicyKey = s);

            webServiceAdaptor.ExceptionPolicy = exceptionPolicy;

            // Act
            webServiceAdaptor.Process(4, MockFailedWebServiceCallVoidReturn);

            // Assert
            Assert.Equal(expected, actualExceptionPolicyKey);
        }

        [Fact]
        public void Process_SuccessfulCallNoRequestParameter_ReturnsValueFromCall()
        {
            // Arrange
            const string expected = ExpectedWebServiceResult;
            var webServiceAdaptor = new WebServiceAdaptorNoRequest<string>();

            // Act
            var actual = webServiceAdaptor.Process(MockSuccessfulWebServiceCallNoRequestParameter);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Process_SuccessfullCallNoRequestParameter_IsSuccessTrue()
        {
            // Arrange
            var webServiceAdaptor = new WebServiceAdaptorNoRequest<string>();

            // Act
            webServiceAdaptor.Process(MockSuccessfulWebServiceCallNoRequestParameter);
            var actual = webServiceAdaptor.IsSuccess;

            // Assert
            Assert.True(actual);
        }

        [Fact]
        public void Process_FailedCallNoRequestParameter_IsSuccessFalse()
        {
            // Arrange
            var webServiceAdaptor = new WebServiceAdaptorNoRequest<string>();

            // Act
            webServiceAdaptor.Process(MockFailedWebServiceCallNoRequestParameter);
            var actual = webServiceAdaptor.IsSuccess;

            // Assert
            Assert.False(actual);
        }

        [Fact]
        public void Process_FailedCallNoRequestParameter_ExceptionPolicyCalled()
        {
            // Arrange
            var webServiceAdaptor = new WebServiceAdaptorNoRequest<string>();
             var exceptionPolicyMock = new Mock<IExceptionPolicy>();
            var exceptionPolicy = exceptionPolicyMock.Object;
            var expected = webServiceAdaptor.ExpceptionPolicyKey;
            string actualExceptionPolicyKey = null;

            exceptionPolicyMock.Setup(policy => policy.HandleException(It.IsAny<Exception>(), It.IsAny<string>()))
                .Callback<Exception, string>((e, s) => actualExceptionPolicyKey = s);

            webServiceAdaptor.ExceptionPolicy = exceptionPolicy;

            // Act
            webServiceAdaptor.Process(MockFailedWebServiceCallNoRequestParameter);

            // Assert
            Assert.Equal(expected, actualExceptionPolicyKey);
        }
        

        #region Test Helper Methods

        private string MockSuccessfulWebServiceCall(int arbitaryParamter)
        {
            return ExpectedWebServiceResult;
        }

        private string MockFailedWebServiceCall(int arbitaryParamter)
        {
            throw new SoapException("Failed", new XmlQualifiedName());
        }

        private void MockSuccessfulWebServiceCallVoidReturn(int arbitaryParamter)
        {
        }

        private void MockFailedWebServiceCallVoidReturn(int arbitaryParamter)
        {
            throw new SoapException("Failed", new XmlQualifiedName());
        }

        private string MockSuccessfulWebServiceCallNoRequestParameter()
        {
            return ExpectedWebServiceResult;
        }

        private string MockFailedWebServiceCallNoRequestParameter()
        {
            throw new SoapException("Failed", new XmlQualifiedName());
        }

        #endregion
    }
}
