﻿using System;
using SeanDuffy.Common.SecureData;
using Xunit;

namespace SeanDuffy.Common.Tests.Unit.SecureData
{
    public class SaltTests
    {
        public void GenerateSalt_CorrectLengthReturned()
        {
            // Arrange
            const int expected = 12;
            // Act
            var salt = Salt.Generate();
            var decodeByte = Convert.FromBase64String(salt);
            var actual = decodeByte.Length;

            // Expected
            Assert.Equal(actual, expected);
        }
    }
}
