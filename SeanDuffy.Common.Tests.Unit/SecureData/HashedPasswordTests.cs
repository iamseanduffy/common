﻿using SeanDuffy.Common.SecureData;
using Xunit;

namespace SeanDuffy.Common.Tests.Unit.SecureData
{
    public class HashedPasswordTests
    {
        private const string PlainText = "PLAINTEXT";
        private const string Salt = "EXAMPLESALT";
        private readonly Sha256Hasher _password;

        public HashedPasswordTests()
        {
            _password = new Sha256Hasher();
        }

        [Fact]
        public void GeneratePassword_ValidSaltSupplied_ReturnedPasswordDoesNotContainsSalt()
        {
            // Act
            var hashedPassword = _password.GenerateHash(PlainText, Salt);
            var actual = hashedPassword.Contains(Salt);

            // Assert
            Assert.False(actual);
        }

        [Fact]
        public void GeneratePassword_ValidSaltSupplied_ReturnedPasswordDoesNotContainOriginalText()
        {
            // Act
            var hashedPassword = _password.GenerateHash(PlainText, Salt);
            var actual = hashedPassword.Contains(PlainText);

            // Assert
            Assert.False(actual);
        }

        [Fact]
        public void GeneratePassword_ValidSaltSupplied_ReturnedPasswordLongerThanOriginalSalt()
        {
            // Arrange
            var expected = Salt.Length;

            // Act
            var hashedPassword = _password.GenerateHash(PlainText, Salt);
            var actual = hashedPassword.Length;

            // Assert
            Assert.True(actual > expected);
        }

        [Fact]
        public void IsMatchingPassword_ValidAttemptSupplied_ReturnsTrue()
        {
            // Arrange
            var hash = _password.GenerateHash(PlainText, Salt);
            const string passwordAttempt = PlainText;

            // Act
            var actual = _password.IsMatching(hash, passwordAttempt, Salt);

            // Assert
            Assert.True(actual);
        }
    }
}
