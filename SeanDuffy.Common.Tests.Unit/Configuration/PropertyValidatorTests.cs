﻿using System.Collections.Generic;
using SeanDuffy.Common.Configuration;
using Xunit;

namespace SeanDuffy.Common.Tests.Unit.Configuration
{
   public class PropertyValidatorTests
   {
      [Fact]
      public void Validate_NoProperties_DynamicConfigNoPropertiesInInterfaceException()
      {
         // Arrange
         var propertyValidation = new ConfigurationPropertyInfoValidator();
         var propertyInfoDtos = new List<ConfigurationPropertyInfo>();

         // Act & Assert
          Assert.Throws<DynamicConfigException>(() => propertyValidation.Validate(propertyInfoDtos));
      }

      [Fact]
      public void Validate_NullProperties_DynamicConfigNoPropertiesInInterfaceException()
      {
         // Arrange
         var propertyValidation = new ConfigurationPropertyInfoValidator();

         // Act & Assert
         Assert.Throws<DynamicConfigException>(() => propertyValidation.Validate(null));
      }

      [Fact]
      public void Validate_UnsupportedProperty_DynamicConfigUnsupportedPropertiesInInterfaceException()
      {
         // Arrange
         var propertyValidation = new ConfigurationPropertyInfoValidator();
         var propertyInfoDtos = new List<ConfigurationPropertyInfo>
            {new ConfigurationPropertyInfo {PropertyName = "Test", PropertyType = typeof (long)}};

         // Act & Assert
         Assert.Throws<DynamicConfigException>(() => propertyValidation.Validate(propertyInfoDtos));
      }

      [Fact]
      public void Validate_SupportedTypes_ExecutionCompletes()
      {
         // Act
         var propertyValidation = new ConfigurationPropertyInfoValidator();
         var propertyInfoDtos = new List<ConfigurationPropertyInfo>
            {
               new ConfigurationPropertyInfo { PropertyName = "Test", PropertyType = typeof(int) }
            };

         // Act
         propertyValidation.Validate(propertyInfoDtos);

         // Assert
         Assert.True(true);
      }
   }
}
