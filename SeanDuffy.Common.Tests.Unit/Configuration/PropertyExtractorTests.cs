﻿using System.Linq;
using SeanDuffy.Common.Configuration;
using Xunit;

namespace SeanDuffy.Common.Tests.Unit.Configuration
{
    public class PropertyExtractorTests
    {
        [Fact]
        public void Process_TypeNotInterface_ThrowsDynamicConfigInvalidTypeException()
        {
            // Arrange
            var propertyExtractor = new InterfacePropertyExtractor();
            var type = typeof (string);

            // Act & Assert
            Assert.Throws<DynamicConfigException>(() => propertyExtractor.GetPropertyInfoDtosFromType(type));
        }

        [Fact]
        public void Process_InterfaceContainsMethods_ThrowsDynamicConfigInvalidTypeException()
        {
            // Arrange
            var propertyExtractor = new InterfacePropertyExtractor();
            var type = typeof (IPropertyExtractorTests_InvalidInterfaceWithMethods);

            // Act & Assert
            Assert.Throws<DynamicConfigException>(() => propertyExtractor.GetPropertyInfoDtosFromType(type));

        }

        [Fact]
        public void Process_ValidInterface_ContainsCorrectPropertyName()
        {
            // Arrange
            const string expected = "TestString";
            var propertyExtractor = new InterfacePropertyExtractor();
            var type = typeof (IPropertyExtractorTests);

            // Act
            var propertyInfoDto = propertyExtractor.GetPropertyInfoDtosFromType(type).FirstOrDefault();
            var actual = propertyInfoDto != null ? propertyInfoDto.PropertyName : string.Empty;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Process_ValidInterface_ContainsCorrectPropertyType()
        {
            // Arrange
            var expected = typeof (string);
            var propertyExtractor = new InterfacePropertyExtractor();
            var type = typeof (IPropertyExtractorTests);

            // Act
            var propertyInfoDto = propertyExtractor.GetPropertyInfoDtosFromType(type).FirstOrDefault();
            var actual = propertyInfoDto != null ? propertyInfoDto.PropertyType : typeof (void);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Process_ValidInterface_ContainsCorrectNumberOfProperties()
        {
            // Arrange
            const int expected = 3;
            var propertyExtractor = new InterfacePropertyExtractor();
            var type = typeof (IPropertyExtractorMultiplePropertiesTests);

            // Act
            var actual = propertyExtractor.GetPropertyInfoDtosFromType(type).Count;

            // Assert
            Assert.Equal(expected, actual);
        }
    }

    public interface IPropertyExtractorTests
    {
        string TestString { get; set; }
    }

    public interface IPropertyExtractorMultiplePropertiesTests
    {
        string TestString { get; set; }
        int TestInt { get; set; }
        bool TestBool { get; set; }
    }

    public interface IPropertyExtractorTests_InvalidInterfaceWithMethods
    {
        string TestString { get; set; }
        string TestMethod();
    }
}