﻿using System;

// http://stackoverflow.com/questions/5686604/equals-implementation-of-nhibernate-entities-unproxy-question
// and nhibernate cookbook
namespace SeanDuffy.Common.Data.NHibernate
{
    public abstract class Entity
    {
        protected internal abstract object Id { get; }

        public override bool Equals(object obj)
        {
            return Equals(obj as Entity);
        }

        private static bool IsTransient(Entity obj)
        {
            return obj != null && obj.Id != null && Equals(obj.Id, GetDefaultValueForType(obj.Id.GetType()));
        }

        private static object GetDefaultValueForType(Type t)
        {
            if (t.IsValueType)
            {
                return Activator.CreateInstance(t);
            }
            return null;
        }

        private Type GetUnproxiedType()
        {
            return GetType();
        }

        public virtual bool Equals(Entity other)
        {
            if (other == null)
                return false;

            if (ReferenceEquals(this, other))
                return true;

            if (!IsTransient(this) && !IsTransient(other) && Equals(Id, other.Id))
            {
                var otherType = other.GetUnproxiedType();
                var thisType = GetUnproxiedType();
                return thisType.IsAssignableFrom(otherType) || otherType.IsAssignableFrom(thisType);
            }

            return false;
        }

        public override int GetHashCode()
        {
            if (Id == null || Equals(Id, GetDefaultValueForType(Id.GetType())))
                return base.GetHashCode();
            return Id.GetHashCode();
        }
    }
}
