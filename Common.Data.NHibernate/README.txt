﻿Create a DataAccess IOC Module like this :
(currently IOC framework is Ninject)

public class DataAccessModule : NHibernateBaseModule
{
    public DataAccessModule(string sessionKey, string connectionStringName)
        : base(
            new StandardSessionFactoryConstructionData(sessionKey, connectionStringName))
    {
    }

    protected override void LoadAdditionalBindings()
    {
        Bind<IInterface>().To<ConcreteType>();
    }
}

Register the module like this from ApplicationStart :

Locator.RegisterModules(new LoanBookDataAccessModule("SessionKey", "ConnectionStringName"));