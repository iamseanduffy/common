﻿using System;
using System.Data;
using System.Linq;
using System.Threading;
using NHibernate;
using SeanDuffy.Common.Data.NHibernate.Extensions;
using SeanDuffy.Common.Data.NHibernate.SessionManagement;
using SeanDuffy.Common.Ioc.Ninject;

namespace SeanDuffy.Common.Data.NHibernate
{
    public class NHibernateRepositoryBase
    {
        protected readonly string SessionKey;

        /// <param name="sessionKey">The session key that will be used to retrieve the ISession object from Ninject</param>
        protected NHibernateRepositoryBase(string sessionKey)
        {
            if (string.IsNullOrEmpty(sessionKey)) throw new ArgumentNullException("sessionKey");
            SessionKey = sessionKey;
        }

        protected virtual ISessionManager SessionManager
        {
            get { return Locator.Get<ISessionManager>(SessionKey); }
        }

        protected virtual INHibernateLogger Logger
        {
            get { return Locator.Get<INHibernateLogger>(); }
        }

        /// <summary>
        /// Resolves ISession using service location on each call
        /// Should never be copied into variables since can return different instances
        /// </summary>
        protected virtual ISession CurrentSession
        {
            get { return SessionManager.GetCurrentSession(); }
        }

        /// <summary>
        /// Wraps action in transaction statements
        /// In case of Rollback automatically resets NHibernate session (CurrentSession will return new instance of it)
        /// </summary>
        /// <param name="action">DB statement executing action</param>
        /// <param name="isolationLevel">Transaction isolation level - defaults to the DB default</param>
        protected void Transact(Action action, IsolationLevel? isolationLevel = null)
        {
            var session = CurrentSession;
            if (session.Transaction.IsActive == false)
            {
                using (var tx = isolationLevel.HasValue ? session.BeginTransaction(isolationLevel.Value) : session.BeginTransaction())
                {
                    try
                    {
                        action.Invoke();
                        tx.Commit();
                        return;
                    }
                    catch
                    {
                        tx.Rollback();
                        //Have to reset session since current one will become broken after any exceptions
                        SessionManager.EndSession();
                        throw;
                    }
                }
            }
            action.Invoke();
        }

        /// <summary>
        /// Wraps action in transaction statements
        /// In case of Rollback automatically resets NHibernate session (CurrentSession will return new instance of it)
        /// </summary>
        /// <param name="retrievalFunction">
        /// Should execute DB statement immediately (e.g. should not return IQueriable). Otherwise will be executed outside transaction.
        /// </param>
        /// <param name="isolationLevel">Transaction isolation level - defaults to the DB default</param>
        /// <typeparam name="TResult">
        /// Should never be of IQueryable type - will cause statement to be executed outside of the transaction wrapper
        /// </typeparam>
        protected TResult Transact<TResult>(Func<TResult> retrievalFunction, IsolationLevel? isolationLevel = null)
        {
            var session = CurrentSession;
            if (session.Transaction.IsActive == false)
            {
                using (var tx = isolationLevel.HasValue ? session.BeginTransaction(isolationLevel.Value) : session.BeginTransaction())
                {
                    try
                    {
                        var entity = retrievalFunction.Invoke();
                        tx.Commit();
                        return entity;
                    }
                    catch
                    {
                        tx.Rollback();
                        //Have to reset session since current one will become broken after any exceptions
                        SessionManager.EndSession();
                        throw;
                    }
                }
            }
            return retrievalFunction.Invoke();
        }

        /// <summary>
        /// Saves the entity by doing an INSERT.
        /// If no active transaction it creates and commits a transaction to do so. Rollsback on error and rethrows exception.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity to save</typeparam>
        /// <param name="entity">The entity to save</param>
        /// <param name="isolationLevel">Transaction isolation level - defaults to the DB default</param>
        protected void Insert<TEntity>(TEntity entity, IsolationLevel? isolationLevel = null) where TEntity : Entity
        {
            var session = CurrentSession;
            Action action = () => session.Save(entity);
            Transact(action, isolationLevel);
        }

        /// <summary>
        /// Mass persist of entities with specific isolation level
        /// </summary>
        /// <param name="entities">Entities to persist</param>
        /// <param name="isolationLevel">Transaction isolation level - defaults to the DB default</param>
        protected void SaveAll(IsolationLevel? isolationLevel = null, params Entity[] entities)
        {
            var ienumerableEntities = entities.ToList();
            Action action = () => ienumerableEntities.ForEach(entity => Save(entity));
            Transact(action, isolationLevel);
        }

        /// <summary>
        /// Mass persist of entities
        /// </summary>
        protected void SaveAll(params Entity[] entities)
        {
            SaveAll(null, entities);
        }

        /// <summary>
        /// Saves or updates the entity, NHibernate determines if the entity needs to be INSERTed or UPDATED base on it's Id.
        /// If no active transaction it creates and commits a transaction to do so. Rollsback on error and rethrows exception.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity to save</typeparam>
        /// <param name="entity">The entity to save</param>
        /// <param name="isolationLevel">Transaction isolation level - defaults to the DB default</param>
        protected void Save<TEntity>(TEntity entity, IsolationLevel? isolationLevel = null) where TEntity : Entity
        {
            Action action = () => CurrentSession.SaveOrUpdate(entity);
            Transact(action, isolationLevel);
        }

        /// <summary>
        /// Will attempt to persist the entity multiple times.
        /// In the event the entity is stale, it will be refreshed with the latest values and hydrated with the dirty properties.
        /// In the event a timeout occurs, it will reattempt to persist the specified amount of times.
        /// </summary>
        /// <returns>Returns a bool to specify whether a retry occured or not.</returns>
        protected bool UpdateWithOverwriteIfStaleAndRetryIfTimeout(Entity entity, int retryAttempts = 3, int timeOutSleepInMs = 1000)
        {
            var retried = false;
            var changeSet = CurrentSession.ExtractDirtyProperties(entity);

            for (var i = 0; i <= retryAttempts; i++)
            {
                try
                {
                    Transact(() => CurrentSession.Update(entity));
                    break;
                }
                catch (StaleObjectStateException ex)
                {
                    Logger.LogWarning("NHibernateRepository", "A StaleObjectException occured trying to update an entity.", ex);

                    if (i == retryAttempts)
                        throw;

                    if (changeSet == null)
                        throw new Exception("Failed to extract dirty properties in order to retry with overwrite", ex);

                    CurrentSession.Refresh(entity);
                    changeSet.ApplyTo(entity);

                    retried = true;
                }
                catch (TimeoutException ex)
                {
                    Logger.LogWarning("NHibernateRepository", "A TimeoutException occured trying to update an entity.", ex);

                    if (i == retryAttempts) throw;

                    Thread.Sleep(timeOutSleepInMs);
                    retried = true;
                }
            }

            return retried;
        }

        /// <summary>
        /// Saves the entity by explicilty UPDATEing the entity.
        /// If no active transaction it creates and commits a transaction to do so. Rollsback on error and rethrows exception.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity to save</typeparam>
        /// <param name="entity">The entity to save</param>
        /// <param name="isolationLevel">Transaction isolation level - defaults to the DB default</param>
        protected void Update<TEntity>(TEntity entity, IsolationLevel? isolationLevel = null) where TEntity : Entity
        {
            var session = CurrentSession;
            Action action = () => session.Update(entity);
            Transact(action, isolationLevel);
        }

        /// <summary>
        /// Retrieves the entity with the given id.
        /// If no active transaction it creates and commits a transaction to do so. Rollsback on error and rethrows exception.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity to retrieve</typeparam>
        /// <param name="id">The key value of the object</param>
        /// <param name="isolationLevel">Transaction isolation level - defaults to the DB default</param>
        protected TEntity Retrieve<TEntity>(object id, IsolationLevel? isolationLevel = null) where TEntity : Entity
        {
            var session = CurrentSession;
            Func<TEntity> function = () => session.Get<TEntity>(id);
            return Transact(function, isolationLevel);
        }

        /// <summary>
        /// Deletes the entity.
        /// If no active transaction it creates and commits a transaction to do so. Rollsback on error and rethrows exception.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity to delete</typeparam>
        /// <param name="entity">The entity to delete</param>
        /// <param name="isolationLevel">Transaction isolation level - defaults to the DB default</param>
        protected void Delete<TEntity>(TEntity entity, IsolationLevel? isolationLevel = null) where TEntity : Entity
        {
            var session = CurrentSession;
            Action action = () => session.Delete(entity);
            Transact(action, isolationLevel);
        }
    }
}
