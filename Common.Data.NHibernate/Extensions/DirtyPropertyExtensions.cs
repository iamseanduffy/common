﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using NHibernate;
using NHibernate.Proxy;

namespace SeanDuffy.Common.Data.NHibernate.Extensions
{
    internal static class DirtyPropertyExtensions
    {
        internal static ChangeSet ExtractDirtyProperties(this ISession session, Entity entity)
        {
            var className = NHibernateProxyHelper.GuessClass(entity).FullName;
            var sessionImplementor = session.GetSessionImplementation();
            var persistenceContext = sessionImplementor.PersistenceContext;
            var persister = sessionImplementor.Factory.GetEntityPersister(className);
            var oldEntry = persistenceContext.GetEntry(entity);

            // Will be true if the entity was created with ISession.Load()
            if ((oldEntry == null) && (entity is INHibernateProxy))
            {
                var proxy = entity as INHibernateProxy;
                var obj = persistenceContext.Unproxy(proxy);
                oldEntry = persistenceContext.GetEntry(obj);
            }

            if (oldEntry == null)
                return null;

            var oldState = oldEntry.LoadedState;
            var currentState = persister.GetPropertyValues(entity, sessionImplementor.EntityMode);

            var dirtyProperties = persister.FindDirty(currentState, oldState, entity, sessionImplementor);

            if (dirtyProperties == null)
                return null;

            var dirtyPropertyNameValueMap = dirtyProperties.ToDictionary(
                    x => persister.PropertyNames[x],
                    x => currentState[x]
                );

            return new ChangeSet(entity.Id, dirtyPropertyNameValueMap);
        }

        internal class ChangeSet
        {
            public object Id { get; set; }
            public IDictionary<string, object> PropertyNameValueMap { get; set; }

            public ChangeSet(object id, IDictionary<string, object> propertyNameValueMap)
            {
                Id = id;
                PropertyNameValueMap = propertyNameValueMap;
            }

            public void ApplyTo(Entity entity)
            {
                if (!Id.Equals(entity.Id))
                    throw new InvalidOperationException(
                        string.Format("Tried to rehydrate EntityId: {0} with dirty properties belonging to EntityId: {1}.",
                        entity.Id, Id));

                var entityType = entity.GetType();
                foreach (var propertyNameValuePair in PropertyNameValueMap)
                {
                    var property = entityType.GetProperty(propertyNameValuePair.Key, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);

                    if (property == null)
                        throw new PropertyNotFoundException(entityType, propertyNameValuePair.Key);

                    property.SetValue(entity, propertyNameValuePair.Value, null);
                }
            }
        }
    }
}
