﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using FluentNHibernate.Cfg;

namespace SeanDuffy.Common.Data.NHibernate.Configuration
{
    public class StandardSessionFactoryConstructionData : ISessionFactoryConstructionData
    {
        public virtual string SessionKey { get; set; }
        public virtual string ConnectionStringName { get; set; }

        /// <summary>
        /// Tells NHibernate to look for the <ClassMap /> definitions in the specified assemblies.
        /// </summary>
        public virtual IEnumerable<Assembly> AddMappingsFromAssemblies { get; set; }

        public StandardSessionFactoryConstructionData(string sessionKey, string connectionStringName, params Type[] exampleMappingClasses)
            : this(sessionKey, connectionStringName, exampleMappingClasses.Select(x => x.Assembly))
        {
        }

        public StandardSessionFactoryConstructionData(string sessionKey, string connectionStringName, IEnumerable<Assembly> addMappingsFromAssemblies)
        {
            SessionKey = sessionKey;
            ConnectionStringName = connectionStringName;
            AddMappingsFromAssemblies = addMappingsFromAssemblies;
        }

        /// <summary>
        /// This is a mechanism for adding additional configuration to the factory.
        /// </summary>
        public virtual FluentConfiguration ApplyAdditionalConfiguration(FluentConfiguration standardConfig)
        {
            return standardConfig;
        }
    }
}
