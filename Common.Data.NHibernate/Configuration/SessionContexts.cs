﻿using System.Runtime.Serialization;

namespace SeanDuffy.Common.Data.NHibernate.Configuration
{
    /// <summary>
    /// For string definitions see source of SessionFactoryImpl.BuildCurrentSessionContext()
    /// </summary>
    public enum SessionContexts
    {
        /// <summary>
        /// Current sessions are tracked by CallContext. You are responsible to bind and unbind
        /// an ISession instance with static methods of class CurrentSessionContext
        /// </summary>
        [DataMember(Name = "call")]
        Call,

        /// <summary>
        /// Current sessions are tracked by HttpContext.
        /// However, you are responsible to bind and unbind an ISession instance
        /// with static methods on this class, it never opens, flushes, or closes an ISession itself
        /// </summary>
        [DataMember(Name = "managed_web")]
        ManagedWeb,

        /// <summary>
        /// Current session is stored in a thread-static variable.
        /// This context only supports one session factory.
        /// You are responsible to bind and unbind an ISession instance with static methods of class CurrentSessionContext
        /// </summary>
        [DataMember(Name = "thread_static")]
        ThreadStatic,

        /// <summary>
        /// Analogous to ManagedWeb above, stores the current session in HttpContext.
        /// You are responsible to bind and unbind an ISession instance
        /// with static methods of class CurrentSessionContext
        /// </summary>
        [DataMember(Name = "web")]
        Web,

        /// <summary>
        /// Current sessions are tracked by WCF OperationContext.
        /// You need to register the WcfStateExtension extension in WCF.
        /// You are responsible to bind and unbind an ISession instance with static methods of class CurrentSessionContext
        /// </summary>
        [DataMember(Name = "wcf_operation")]
        WcfOperation
    }
}