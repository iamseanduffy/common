﻿using System.Collections.Generic;
using System.Reflection;
using FluentNHibernate.Cfg;

namespace SeanDuffy.Common.Data.NHibernate.Configuration
{
    public interface ISessionFactoryConstructionData
    {
        /// <summary>
        /// Defines unique per DB connection session name
        /// Used as a named binding parameter when Session implementation is bound
        /// </summary>
        string SessionKey { get; }

        /// <summary>
        /// ConnectionStringName to be used for specified SessionKey
        /// </summary>
        string ConnectionStringName { get; }

        /// <summary>
        /// Tells NHibernate to look for the <ClassMap /> definitions in the specified assemblies.
        /// </summary>
        IEnumerable<Assembly> AddMappingsFromAssemblies { get; }

        /// <summary>
        /// This is a mechanism for adding additional configuration to the factory.
        /// </summary>
        FluentConfiguration ApplyAdditionalConfiguration(FluentConfiguration standardConfig);
    }
}