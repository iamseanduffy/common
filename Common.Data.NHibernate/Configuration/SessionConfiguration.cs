﻿using System;
using System.Collections.Generic;
using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;

namespace SeanDuffy.Common.Data.NHibernate.Configuration
{
    public static class SessionConfiguration
    {
        public const string SessionFactoryPropertiesConfigSectionName = "SessionFactoryProperties";

        public static FluentConfiguration CreateFluentConfiguration(MsSqlConfiguration msSqlConfiguration, 
            IEnumerable<Assembly> fluentMappingAssemblies, Action<MsSqlConnectionStringBuilder> connectionStringBuilder, 
            string currentSessionContextClass)
        {
            var fluentConfig = Fluently.Configure().Database(msSqlConfiguration.ConnectionString(connectionStringBuilder));
            // ReSharper disable LoopCanBeConvertedToQuery
            if (fluentMappingAssemblies != null)
                foreach (var fluentMappingAssembly in fluentMappingAssemblies)
                // ReSharper restore LoopCanBeConvertedToQuery
                {
                    var assembly = fluentMappingAssembly;
                    fluentConfig.Mappings(f => f.FluentMappings.AddFromAssembly(assembly));
                }

            fluentConfig.ExposeConfiguration(c => c.SetProperty("current_session_context_class", currentSessionContextClass));
           
            //if (Settings.Default.SecondLevelCacheEnabled)
            //{
            //    if (Settings.Default.UseScaleOutAsCacheProvider)
            //    {
            //        fluentConfig
            //            .Cache(c => c.UseQueryCache()
            //                            .UseSecondLevelCache()
            //                            .ProviderClass<SossCacheProvider>()
            //                            .UseMinimalPuts()
            //                            .RegionPrefix(cachePrefixName))
            //            .ExposeConfiguration(c => c.SetProperty("expiration", (Settings.Default.SecondLevelCacheTimeoutInMinutes * 60).ToString()))
            //            .ExposeConfiguration(c => c.SetProperty("expirationType", "absolute"));
            //    }
            //    else
            //    {
            //        fluentConfig
            //            .Cache(c => c.UseQueryCache()
            //                            .UseSecondLevelCache()
            //                            .ProviderClass<SysCacheProvider>()
            //                            .UseMinimalPuts()
            //                            .RegionPrefix(cachePrefixName))
            //            .ExposeConfiguration(c => c.SetProperty("expiration", (Settings.Default.SecondLevelCacheTimeoutInMinutes * 60).ToString()));
            //    }
            //}

            //var sessionFactoryProperties = ConfigurationManagerExtensions.GetCustomAppSettings(SessionFactoryPropertiesConfigSectionName);
            //if (sessionFactoryProperties != null)
            //{
            //    foreach (var property in sessionFactoryProperties)
            //    {
            //        string propertyKey = property.Key, propertyValue = property.Value;
            //        fluentConfig.ExposeConfiguration(cfg => cfg.SetProperty(propertyKey, propertyValue));
            //    }
            //}

            return fluentConfig;
        }

        /// <summary>
        /// Defaults to MsSQL2008 configuration. Creates basic fluent configuration.
        /// </summary>
        /// <param name="fluentMappingAssemblies">The assemblies to load all ClassMaps from. You can set this to null to not use assembly mapping.</param>
        /// <param name="connectionStringBuilder"></param>
        /// <param name="currentSessionContextClass"></param>
        /// <param name="cachePrefixName"> </param>
        /// <returns></returns>
        public static FluentConfiguration CreateFluentConfiguration(IEnumerable<Assembly> fluentMappingAssemblies, Action<MsSqlConnectionStringBuilder> connectionStringBuilder, string currentSessionContextClass)
        {
            return CreateFluentConfiguration(MsSqlConfiguration.MsSql2008, fluentMappingAssemblies, connectionStringBuilder, currentSessionContextClass);
        }

        public static FluentConfiguration CreateFluentConfiguration(IEnumerable<Assembly> fluentMappingAssemblies, string connectionStringName, string currentSessionContextClass)
        {
            return CreateFluentConfiguration(MsSqlConfiguration.MsSql2008, fluentMappingAssemblies, connectionStringName, currentSessionContextClass);
        }

        public static FluentConfiguration CreateFluentConfiguration(MsSqlConfiguration msSqlConfiguration, IEnumerable<Assembly> fluentMappingAssemblies, string connectionStringName, string currentSessionContextClass)
        {
            Action<MsSqlConnectionStringBuilder> connectionStringBuilder = c => c.FromConnectionStringWithKey(connectionStringName);
            return CreateFluentConfiguration(msSqlConfiguration, fluentMappingAssemblies, connectionStringBuilder, currentSessionContextClass);
        }
    }
}
