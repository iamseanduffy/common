﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using SeanDuffy.Common.Ioc.Ninject;

namespace SeanDuffy.Common.Data.NHibernate.SessionManagement
{
    public class NHibernateWcfBehaviorExtension : BehaviorExtensionElement
    {
        public override Type BehaviorType
        {
            get { return typeof(NHibernateWcfBehaviorExtension); }
        }

        protected override object CreateBehavior()
        {
            return new NHibernateWcfBehaviorExtension();
        }

        public void AddBindingParameters(
            ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase,
            Collection<ServiceEndpoint> endpoints,
            BindingParameterCollection bindingParameters)
        {
        }

        public void ApplyDispatchBehavior(
            ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase)
        {
            // get a list of currently defined NHibernateSessionManager's
            var sessionManagers = Locator.GetAll<ISessionManager>().ToList();

            // if any NHibernateSessionManager's are defined then add a NHibernateSessionManagerWcfIDispatchMessageInspector to manage them
            if (sessionManagers.Any())
            {
                foreach (var channelDispatcherBase in serviceHostBase.ChannelDispatchers)
                {
                    var channelDispatcher = (ChannelDispatcher)channelDispatcherBase;
                    foreach (var ed in channelDispatcher.Endpoints)
                    {
                        var messageInspector = new NHibernateWcfIDispatchMessageInspector(sessionManagers);
                        ed.DispatchRuntime.MessageInspectors.Add(messageInspector);
                    }
                }
            }
        }

        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
        }
    }
}
