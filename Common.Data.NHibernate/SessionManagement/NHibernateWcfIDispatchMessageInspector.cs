﻿using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;

namespace SeanDuffy.Common.Data.NHibernate.SessionManagement
{
    public class NHibernateWcfIDispatchMessageInspector : IDispatchMessageInspector
    {
        private readonly IEnumerable<ISessionManager> _sessionManagers;

        public NHibernateWcfIDispatchMessageInspector(IEnumerable<ISessionManager> sessionManagers)
        {
            _sessionManagers = sessionManagers;
        }

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            return null;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            foreach (var sessionManager in _sessionManagers)
            {
                sessionManager.EndSession();
            }
        }
    }
}
