﻿namespace SeanDuffy.Common.Data.NHibernate.SessionManagement
{
    /// <summary>
    /// Esposes functionality to clean open sesisons to the database.
    /// </summary>
    public interface IDbSessionCleaner
    {
        /// <summary>
        /// Ends all currently opened sessions to the database.
        /// </summary>
        void EndAllSessions();
    }
}
