﻿using SeanDuffy.Common.Ioc.Ninject;

namespace SeanDuffy.Common.Data.NHibernate.SessionManagement
{
    public class NHibernateSessionCleaner : IDbSessionCleaner
    {
        public void EndAllSessions()
        {
            var sessionManagers = Locator.GetAll<ISessionManager>();
            foreach (var sessionManager in sessionManagers)
            {
                sessionManager.EndSession();
            }
        }
    }
}
