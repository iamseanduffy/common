﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using System.Threading;
using System.Web;
using NHibernate;
using NHibernate.Context;
using SeanDuffy.Common.Data.NHibernate.Configuration;
using SeanDuffy.Common.Extensions;

namespace SeanDuffy.Common.Data.NHibernate.SessionManagement
{
    public sealed class NHibernateSessionManager : ISessionManager
    {
        private readonly ISessionFactoryConstructionData _sessionFactoryConstructionData;
        private readonly IDictionary<SessionContexts, Lazy<ISessionFactory>> _sessionFactories;
        private readonly Lazy<bool> _isLoggingEnabled;

        public NHibernateSessionManager(ISessionFactoryConstructionData sessionFactoryConstructionData)
        {
            _sessionFactoryConstructionData = sessionFactoryConstructionData;
            _sessionFactories = CreateSessionFactories();

            _isLoggingEnabled = new Lazy<bool>(() => Convert.ToBoolean(ConfigurationManager.AppSettings["EnableNHibernateSessionLogging"]));
            WriteLog("created a new NHibernateSessionManager");
        }

        private INHibernateLogger Logger
        {
            get { throw new NotImplementedException(); }
        }

        public ISession GetCurrentSession()
        {
            WriteLog("requested current session");

            var sessionFactory = _sessionFactories[GetCurrentSessionContext()].Value;
            if (CurrentSessionContext.HasBind(sessionFactory) == false)
            {
                CurrentSessionContext.Bind(sessionFactory.OpenSession());
                WriteLog("no session bound, opened and bound a new one");
            }

            return sessionFactory.GetCurrentSession();
        }

        public void EndSession()
        {
            WriteLog("end session called");

            if (_sessionFactories[GetCurrentSessionContext()].IsValueCreated == false)
                return;

            var sessionFactory = _sessionFactories[GetCurrentSessionContext()].Value;
            var oldSession = CurrentSessionContext.Unbind(sessionFactory);
            if (oldSession != null)
            {
                WriteLog("active session found and disposed");
                oldSession.Dispose();
            }
        }

        private static SessionContexts GetCurrentSessionContext()
        {
            if (OperationContext.Current != null)
                return SessionContexts.WcfOperation;

            if (HttpContext.Current != null)
                return SessionContexts.Web;

            return SessionContexts.Call;
        }

        private ISessionFactory CreateSessionFactory(SessionContexts sessionContext)
        {
            var connectionString = ConfigurationManager.ConnectionStrings[_sessionFactoryConstructionData.ConnectionStringName].ConnectionString;
            var config = SessionConfiguration.CreateFluentConfiguration
            (
                _sessionFactoryConstructionData.AddMappingsFromAssemblies,
                _sessionFactoryConstructionData.ConnectionStringName,
                sessionContext.ToDescriptionString()
                );

            WriteLog("created new SessionFactory");

            return _sessionFactoryConstructionData.ApplyAdditionalConfiguration(config).BuildSessionFactory();
        }

        private IDictionary<SessionContexts, Lazy<ISessionFactory>> CreateSessionFactories()
        {
            var factories = new ConcurrentDictionary<SessionContexts, Lazy<ISessionFactory>>();

            factories.TryAdd(SessionContexts.Web, new Lazy<ISessionFactory>(() =>
                CreateSessionFactory(SessionContexts.Web), LazyThreadSafetyMode.ExecutionAndPublication));

            factories.TryAdd(SessionContexts.WcfOperation, new Lazy<ISessionFactory>(() =>
                CreateSessionFactory(SessionContexts.WcfOperation), LazyThreadSafetyMode.ExecutionAndPublication));

            factories.TryAdd(SessionContexts.ThreadStatic, new Lazy<ISessionFactory>(() =>
                CreateSessionFactory(SessionContexts.ThreadStatic), LazyThreadSafetyMode.ExecutionAndPublication));

            factories.TryAdd(SessionContexts.Call, new Lazy<ISessionFactory>(() =>
                CreateSessionFactory(SessionContexts.Call), LazyThreadSafetyMode.ExecutionAndPublication));

            return factories;
        }

        private void WriteLog(string message)
        {
            if (_isLoggingEnabled.Value == false)
                return;

            var msg = string.Format("{0} -- {1}, {2}", message, GetCurrentSessionContext().ToDescriptionString(), _sessionFactoryConstructionData.SessionKey);
            Logger.LogDebug("NHibernateSessionManager", msg, null);
        }
    }
}
