﻿using NHibernate;

namespace SeanDuffy.Common.Data.NHibernate.SessionManagement
{
    public interface ISessionManager
    {
        ISession GetCurrentSession();
        void EndSession();
    }
}