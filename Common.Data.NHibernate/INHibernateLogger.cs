﻿using System;

namespace SeanDuffy.Common.Data.NHibernate
{
    public interface INHibernateLogger
    {
        void LogWarning(string logType, string message, Exception exception);
        void LogDebug(string logType, string message, Exception exception);
    }
}
