﻿using System;
using Ninject;
using Ninject.Modules;
using SeanDuffy.Common.Data.NHibernate.Configuration;
using SeanDuffy.Common.Data.NHibernate.SessionManagement;

namespace SeanDuffy.Common.Data.NHibernate.Ninject
{
    public abstract class NHibernateBaseModule : NinjectModule
    {
        private NHibernateSessionManager _sessionManager;
        private readonly ISessionFactoryConstructionData _sessionFactoryConstructionData;

        protected string SessionKey { get; set; }
        protected event Action OnBeginLoad;

        protected NHibernateBaseModule(string sessionKey, string connectionStringName, params Type[] exampleMappingClasses)
            : this(new StandardSessionFactoryConstructionData(sessionKey, connectionStringName, exampleMappingClasses))
        {
        }

        protected NHibernateBaseModule(ISessionFactoryConstructionData sessionFactoryConstructionData)
        {
            SessionKey = sessionFactoryConstructionData.SessionKey;
            _sessionFactoryConstructionData = sessionFactoryConstructionData;

            OnBeginLoad += BindSessionManager;
            OnBeginLoad += LoadAdditionalBindings;
        }

        public sealed override void Load()
        {
            OnBeginLoad.Invoke();
        }

        protected abstract void LoadAdditionalBindings();

        protected virtual void BindSessionManager()
        {
            if (Kernel.TryGet<ISessionManager>(SessionKey) != null)
                return;

            if (_sessionManager == null)
                _sessionManager = new NHibernateSessionManager(_sessionFactoryConstructionData);

            Bind<ISessionManager>()
                .ToConstant(_sessionManager)
                .InSingletonScope()
                .Named(SessionKey);

            if (Kernel.TryGet<IDbSessionCleaner>() != null)
                return;

            Bind<IDbSessionCleaner>().To<NHibernateSessionCleaner>().InSingletonScope();
        }
    }
}