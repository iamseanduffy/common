﻿using Xunit;

namespace SeanDuffy.Common.Ioc.Ninject.Tests
{
    public class LocatorTests
    {
        public LocatorTests()
        {
            Locator.SetKernel();
        }

        [Fact]
        public void Get_BindInterfaceToConcreteType_ValueIsNotNull()
        {
            // Arrange
            Locator.Bind<ITestInterface>().To<TestClass>();

            // Act
            var actual = Locator.Get<ITestInterface>();

            // Assert
            Assert.NotNull(actual);
        }

        [Fact]
        public void Get_BindInterfaceToConcreteType_ValueIsCorrectType()
        {
            // Arrange
            Locator.Bind<ITestInterface>().To<TestClass>();

            // Act
            var actual = Locator.Get<ITestInterface>();

            // Assert
            Assert.IsType<TestClass>(actual);
        }

        interface ITestInterface
        {
            
        }

        class TestClass : ITestInterface
        {
            
        }
    }
    
}
