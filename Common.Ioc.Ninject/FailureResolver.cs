﻿using System;
using System.Collections.Generic;
using Ninject;
using Ninject.Activation;
using Ninject.Infrastructure;
using Ninject.Planning.Bindings;
using Ninject.Planning.Bindings.Resolvers;

namespace SeanDuffy.Common.Ioc.Ninject
{
    class FailureResolver : IMissingBindingResolver
    {
        public void Dispose() { }

        public INinjectSettings Settings { get; set; }

        public IEnumerable<IBinding> Resolve(Multimap<Type, IBinding> bindings, IRequest request)
        {
            Locator.ResetKernel();
            return null;
        }
    }
}