﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ninject;
using Ninject.Modules;
using Ninject.Parameters;
using Ninject.Planning.Bindings.Resolvers;
using Ninject.Syntax;

namespace SeanDuffy.Common.Ioc.Ninject
{
    public static class Locator
    {
        private static INinjectModule[] _modules;
        private static INinjectSettings _settings;

        private static readonly object KernelLockToken = new object();
        private static readonly object ResettingKernelLockToken = new object();
        private static volatile bool _resettingKernel;

        internal static void ResetKernel()
        {
            if (!_resettingKernel)
            {
                lock (ResettingKernelLockToken)
                {
                    if (!_resettingKernel)
                    {
                        _resettingKernel = true;
                        RegisterModules(_settings, _modules);
                        _resettingKernel = false;
                    }
                }
            }
        }

        public static void RegisterModules(INinjectSettings settings, params INinjectModule[] modules)
        {
            _settings = settings;
            _modules = modules;

            if (_settings != null)
                SetKernel(new StandardKernel(settings, modules));
            else
                SetKernel(new StandardKernel(modules));
        }

        public static void RegisterModules(params INinjectModule[] modules)
        {
            RegisterModules(null, modules);
        }

        public static void SetKernel(IKernel kernel = null)
        {
            Kernel = kernel ?? new StandardKernel();
        }

        public static void AutoResetKernelOnMissingBindingDoNotCallThisFromMVCProjects()
        {
            Kernel.Components.Add<IMissingBindingResolver, FailureResolver>();
        }

        public static object Get(Type type)
        {
            return Kernel.Get(type);
        }

        public static T Get<T>()
        {
            return Kernel.Get<T>();
        }

        public static T GetFromStaticCache<T>()
        {
            return Get<T>();
        }

        /// <summary>
        /// Example GetWithConstructorArguments&lt;MyUnmappedClass&gt;(new { ln = loanDAL.Retrieve(1) })
        /// Where MyUnmappedClass has following constructor:
        /// MyUnmappedClass(loan ln, IPersonDAL personDal, ICardDAL cardDal)
        /// In this case: IPersonDAL and ICardDAL will be instantiated using NInject module mappings,
        /// and ln will be passed through
        /// </summary>
        public static T GetWithConstructorArguments<T>(object additionalParamsWrapper)
        {
            var ctorParams = additionalParamsWrapper.GetType().GetProperties()
                .Select(propertyInfo =>
                    new ConstructorArgument(
                        propertyInfo.Name,
                        propertyInfo.GetValue(additionalParamsWrapper, null)
                    )
                )
                .ToArray();

            return Kernel.Get<T>(ctorParams);
        }

        public static T Get<T>(Enum name)
        {
            return Kernel.Get<T>(name.ToString());
        }

        public static T Get<T>(string name)
        {
            return Kernel.Get<T>(name);
        }

        public static object Get(Type type, string name)
        {
            return Kernel.Get(type, name);
        }

        public static IEnumerable<T> GetAll<T>()
        {
            return Kernel.GetAll<T>();
        }

        public static IEnumerable<object> GetAll(Type type)
        {
            return Kernel.GetAll(type);
        }

        public static IEnumerable<T> GetAll<T>(Enum name)
        {
            return Kernel.GetAll<T>(name.ToString());
        }

        public static IEnumerable<T> GetAll<T>(string name)
        {
            return Kernel.GetAll<T>(name);
        }

        private static IKernel _kernel;

        public static IBindingToSyntax<T> Bind<T>()
        {
            return Kernel.Bind<T>();
        }

        public static IKernel Kernel
        {
            get
            {
                lock (KernelLockToken)
                {
                    return _kernel;
                }
            }
            private set
            {
                lock (KernelLockToken)
                {
                    _kernel = value;
                }
            }
        }
    }
}
