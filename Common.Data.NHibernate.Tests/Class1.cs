﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using SeanDuffy.Common.Data.NHibernate;
using SeanDuffy.Common.Data.NHibernate.Ninject;
using SeanDuffy.Common.Data.NHibernate.SessionManagement;
using Xunit;

namespace Common.Data.NHibernate.Tests
{
    public class SessionManagerTest : ISessionManager
    {
        public ISession GetCurrentSession()
        {
            throw new NotImplementedException();
        }

        public void EndSession()
        {
            throw new NotImplementedException();
        }
    }

    public class RepositoryTest : NHibernateRepositoryBase
    {
        public RepositoryTest() : base("TestSession")
        {
        }
    }

     public class LoanBookDataAccessModule : NHibernateBaseModule
    {
        public LoanBookDataAccessModule(string sessionKey, string connectionStringName)
            : base(new EventListenerSessionFactoryConstructionData(sessionKey, connectionStringName, typeof(EntryMap), typeof(SettingMap)))
        {
        }

        protected override void LoadAdditionalBindings()
        {
    
    public class Class1
    {
        [Fact]
        public void Test()
        {
            var sessionManager = new SessionManagerTest();

        }
    }
}
