﻿using SeanDuffy.Common.Configuration;
using Xunit;

namespace SeanDuffy.Common.Tests.Integration.Configuration
{
    public class DynamicConfigTests
    {
        [Fact]
        public void GenerateConfig_SimpleString_ReturnsOk()
        {
            // Arrange
            const string expected = "success";
            var dynamicConfig = new TestConfig();
            var dynamicType = dynamicConfig.GenerateConfig(true);

            // Act
            var actual = dynamicType.TestString;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GenerateConfig_SimpleBool_ReturnsOk()
        {
            // Arrange
            const bool expected = true;
            var dynamicConfig = new TestConfig();
            var dynamicType = dynamicConfig.GenerateConfig(true);

            // Act
            bool actual = dynamicType.TestBool;

            // Assert
            Assert.Equal(actual, expected);
        }

        [Fact]
        public void GenerateConfig_SimpleInt_ReturnsOk()
        {
            // Arrange
            const int expected = 666;
            var dynamicConfig = new TestConfig();
            var dynamicType = dynamicConfig.GenerateConfig(true);

            // Act
            var actual = dynamicType.TestInt;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GenerateConfig_IncompletedConfigNotAllowed_ThrowsDynamicConfigIncompleteConfigurationException()
        {
            // Arrange
            var dynamicConfig = new TestConfig();

            // Act & Assert
            Assert.Throws<DynamicConfigException>(() => dynamicConfig.GenerateConfig());
        }
    }

    public interface IDynamicConfigTest
    {
        string TestString { get; }
        bool TestBool { get; }
        int TestInt { get; }
        int TestMissingInt { get; }
    }
}
